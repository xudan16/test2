
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
#      when: never
    - when: always

stages:
  - test
  - build
  - deploy
  - delete

default:
  tags:
    - gitlab-org-docker
  image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/ci-image:v1.0.7

variables:
  DOCKER_TLS_CERTDIR: ""
  GITLEAKS_ARGS: '--verbose --log-opts=$CI_COMMIT_SHA'
  DOCKER_HOST: tcp://docker:2375/
  CAPO_PLATFORM_TAG: capo-ci
  OCI_TAG_FORMAT: "0.0.0-git-$CI_COMMIT_SHORT_SHA"

include:
  - project: "to-be-continuous/gitleaks"
    ref: 2.2.0
    file: "templates/gitlab-ci-gitleaks.yml"

.generator-base:
  stage: test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - charts/**/*
        - kustomize-units/**/*
        - environment-values/**/*
        - tools/gci-templates/**/*
        - tools/yaml2json.py

chart-generator:
  extends: .generator-base
  script:
    - echo "Generate CI jobs for each modified charts/units/environment-values"
    - ./tools/gci-templates/scripts/generate-pipeline.sh > generated-pipeline.yml
  artifacts:
    expire_in: 1 hour
    paths:
      - generated-pipeline.yml

chart-jobs:
  extends: .generator-base
  needs:
    - chart-generator
  trigger:
    include:
      - artifact: generated-pipeline.yml
        job: chart-generator
    strategy: depend

.lint-base:
  stage: test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

yamllint:
  extends: .lint-base
  script:
    - 'yamllint . -d "$(cat < tools/gci-templates/configuration/yamllint.yaml) $(cat < tools/gci-templates/configuration/yamllint-helm-exclude-charts.yaml)"'

test-echo-envs:
  stage: test
  script:
    - echo $CI_PIPELINE_SOURCE
    - echo $CI_COMMIT_BRANCH
    - echo $CI_OPEN_MERGE_REQUESTS
    - echo "update"

avoid-typo-on-bootstrap:
  extends: .lint-base
  script:
    - |
      rm -rf .git  # because busybox grep does not support --exclude-dir
      echo "Check against frequent typos on 'bootstrap'..."
      set +e
      typos=$(grep -rnsiE 'boostrap|bootrap|bootsrap' . | grep -v '.gitlab-ci.yaml:      typos=')
      set -e
      if [ -n "$typos" ]; then
        echo "A few typos were found on the 'bootstrap' word:"
        echo "-----------------"
        echo "$typos"
        echo "-----------------"
        exit 1
      fi

check-docs-markdown:
  extends: .lint-base
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.16-vale-2.20.2-markdownlint-0.32.2-markdownlint2-0.5.1
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - |
      md_files=$(git diff --name-only $CI_COMMIT_SHA origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep "\.md$" || true)
      if [ -n "$md_files" ] ; then
        markdownlint-cli2-config tools/gci-templates/configuration/.markdownlint.yml $md_files
      else
        echo "No modified .md files"
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        - "**/*.md"

.deploy-base:
  stage: deploy
  services:
    - name: docker:23.0.6-dind
      alias: docker
  artifacts:
    expire_in: 6 hour
    when: always
    paths:
      - debug-on-exit.log
      - bootstrap-cluster-dump/
      - management-cluster-dump/
      - bootstrap-cluster-units-report.xml
      - management-cluster-units-report.xml
      - bootstrap-timeline.html
      - management-cluster-timeline.html
    reports:
      junit:
      - bootstrap-cluster-units-report.xml
      - management-cluster-units-report.xml
  needs:
    - job: push-helm-artifacts
      optional: true
    - job: publish-kustomize-units-artifact
      optional: true
    - job: publish-sylva-units-artifact
      optional: true

.common-deployment:
  script_common:
    - echo -e "\e[1m\e[0Ksection_start:`date +%s`:gitlab_ci_common[collapsed=true]\r\e[0KRunning common deployment steps\e[0m"
    - export DOCKER_IP=$(getent ahostsv4 docker | awk '{print $1}' | sort -u)
    - export ENV_PATH=environment-values/${ENV_NAME}
    - values_file=environment-values/${ENV_NAME}/values.yaml
    - docker network create --attachable kind
    - KIND_PREFIX=$(docker network inspect kind -f '{{ (index .IPAM.Config 0).Subnet }}')
    - ip route add $KIND_PREFIX via $DOCKER_IP
    - export KIND_CLUSTER_NAME=bootstrap-${CI_PIPELINE_ID}-${ENV_NAME}
    - export MANAGEMENT_CLUSTER_NAME="management-cluster-${CI_PIPELINE_ID}-${ENV_NAME}"
    - export TEST_WORKLOAD_CLUSTER_NAME="first-workload-cluster-${CI_PIPELINE_ID}-${ENV_NAME}"
    - |
      if [ ${OCI_TAG} ]; then
        echo -e "Applying OCI configuration for tag: ${OCI_TAG}"
        export KIND_CLUSTER_NAME="bootstrap-${CI_PIPELINE_ID}-${ENV_NAME}-oci"
        export MANAGEMENT_CLUSTER_NAME="management-cluster-${CI_PIPELINE_ID}-${ENV_NAME}-oci"
        export TEST_WORKLOAD_CLUSTER_NAME="first-workload-cluster-${CI_PIPELINE_ID}-${ENV_NAME}-oci"
        if [ $(yq '.components | length' environment-values/$ENV_NAME/kustomization.yaml) -eq "0" ]; then yq -i '.components = []' environment-values/$ENV_NAME/kustomization.yaml; fi
        yq -i '.components += "../components/oci-artifacts"' $ENV_PATH/kustomization.yaml
      cat <<EOF>> $ENV_PATH/kustomization.yaml
      patches:
      - target:
          kind: HelmRelease
          name: sylva-units
        patch: |
          - op: replace
            path: /spec/chart/spec/version
            value: ${OCI_TAG}
      EOF
        cat $ENV_PATH/kustomization.yaml
        export METALLB_HELM_OCI_URL='{{ lookup "source.toolkit.fluxcd.io/v1beta2" "HelmRepository" "default" "sylva-core" | dig "spec" "url" "oci://registry.gitlab.com/sylva-projects/sylva-core" | dir | replace "oci:/" "oci://" }}/sylva-core/metallb'
        yq -i '.metallb_helm_oci_url = strenv(METALLB_HELM_OCI_URL)' $values_file
      fi
    - yq -i '.cluster.name = strenv(MANAGEMENT_CLUSTER_NAME)' $values_file
    - yq -i '.cluster.test_workload_cluster_name = strenv(TEST_WORKLOAD_CLUSTER_NAME)' $values_file
    - yq -i '.env_type_ci = true' $values_file
    - echo -e "\e[0Ksection_end:`date +%s`:gitlab_ci_common\r\e[0K"
  script_capd:
    - echo -e "\e[1m\e[0Ksection_start:`date +%s`:gitlab_ci_capd[collapsed=true]\r\e[0KApplying specific capd configuration\e[0m"
    - export DOCKER_HOST=tcp://$DOCKER_IP:2375
    - yq -i '.cluster.capd.docker_host = strenv(DOCKER_HOST)' $values_file
    - export CLUSTER_EXTERNAL_IP=$(echo $KIND_PREFIX | awk -F"." '{print $1"."$2"."$3".100"}')
    - yq -i '.cluster.cluster_external_ip = strenv(CLUSTER_EXTERNAL_IP)' $values_file
    - echo -e "\e[0Ksection_end:`date +%s`:gitlab_ci_capd\r\e[0K"
  script_capo:
    - echo -e "\e[1m\e[0Ksection_start:`date +%s`:gitlab_ci_capo[collapsed=true]\r\e[0KApplying specific capo configuration\e[0m"
    - yq -i eval-all 'select(fileIndex==0).cluster.capo.clouds_yaml = select(fileIndex==1) | select(fileIndex==0)' environment-values/${ENV_NAME}/secrets.yaml ~/.config/openstack/clouds.yml
    - echo "CAPO_TAG=${CAPO_TAG}"
    - export CAPO_TAG="${CAPO_TAG}"
    - yq -i '.cluster.capo.resources_tag = strenv(CAPO_TAG)' $values_file
    - git config --global credential.helper cache
    # the CI_COMPONENT_* variables below are set in the gitlab runner configuration
    # and allow to enable extra kustomize components needed in the context of the platform
    # on which this test actually runs
    - git clone --depth=1 https://foo:${CI_COMPONENT_REPO_PAT}@${CI_COMPONENT_REPO}
    - export CI_COMPONENT="https://${CI_COMPONENT_REPO}//${CI_COMPONENT_REPO_PATH}-${ENV_NAME}?ref=2023-06-22"
    - if [ $(yq '.components | length' environment-values/$ENV_NAME/kustomization.yaml) -eq "0" ]; then yq -i '.components = []' environment-values/$ENV_NAME/kustomization.yaml; fi
    - yq -i '.components += strenv(CI_COMPONENT)' environment-values/$ENV_NAME/kustomization.yaml
    - yq -e '.registry_mirrors' $(basename ${CI_COMPONENT_REPO} .git)/${CI_COMPONENT_REPO_PATH}-base/values.yaml > registry_mirrors.yml
    - yq -i eval-all 'select(fileIndex==0).registry_mirrors = select(fileIndex==1) | select(fileIndex==0)' $values_file registry_mirrors.yml
    - echo -e "\e[0Ksection_end:`date +%s`:gitlab_ci_capo\r\e[0K"
  post_deployment_common:
    - kubectl --kubeconfig management-cluster-kubeconfig get pod --selector job-name=check-rancher-clusters-job -o=jsonpath='{.items[?(@.status.phase=="Succeeded")].metadata.name}' | xargs --no-run-if-empty -n1 kubectl --kubeconfig management-cluster-kubeconfig logs || true
  os_cloud:
    # the OS_* variables below are set in the gitlab runner configuration
    # and contain information to connect to the OpenStack instance used
    # by this test
    - mkdir -p ~/.config/openstack
    - |
      cat <<EOF>> ~/.config/openstack/clouds.yml
      clouds:
        capo_cloud:
          auth:
            auth_url: '${OS_AUTH_URL}'
            user_domain_name: '${OS_USER_DOMAIN_ID}'
            project_domain_name: '${OS_PROJECT_DOMAIN_ID}'
            project_name: '${OS_TENANT_NAME}'
            username: '${OS_USERNAME}'
            password: '${OS_PASSWORD}'
          region_name: '${OS_REGION_NAME}'
          verify: false
      EOF
  rules:
    - if: '$CI_COMMIT_TAG && $OCI_TAG'
      variables:
        OCI_TAG: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == 'web'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_DEPLOYMENTS'
    - if: $CI_MERGE_REQUEST_LABELS =~ /run-e2e-tests/
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      allow_failure: true
      when: manual

deploy-preview-capd:
  extends: .deploy-base
  timeout: 15min
  script:
    - !reference [.common-deployment, script_common]
    - !reference [.common-deployment, script_capd]
    - ./preview.sh environment-values/${ENV_NAME}
  artifacts:
    when: on_failure
    expire_in: 1 hour
    paths:
      - debug-on-exit.log
      - bootstrap-cluster-dump/
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - preview.sh
        - tools/shell-lib/common.sh
  variables:
    ENV_NAME: kubeadm-capd

deploy-preview-capd-oci:
  extends: deploy-preview-capd
  variables:
    ENV_NAME: kubeadm-capd
    OCI_TAG: $OCI_TAG_FORMAT

deploy-kubeadm-capd:
  timeout: 60min
  extends: .deploy-base
  script:
    - !reference [.common-deployment, script_common]
    - !reference [.common-deployment, script_capd]
    - ./bootstrap.sh environment-values/${ENV_NAME}
    - !reference [.common-deployment, post_deployment_common]
  rules:
    - !reference [.common-deployment, rules]
  variables:
    ENV_NAME: kubeadm-capd
  artifacts:
    expose_as: kubeadm-capd

deploy-kubeadm-capd-opensuse:
  image: registry.gitlab.com/xudan16/ci-image-opensuse:v0.1
  timeout: 60min
  extends: .deploy-base
  script:
    - !reference [.common-deployment, script_common]
    - !reference [.common-deployment, script_capd]
    - ./bootstrap.sh environment-values/${ENV_NAME}
    - !reference [.common-deployment, post_deployment_common]
  rules:
    - !reference [.common-deployment, rules]
  variables:
    ENV_NAME: kubeadm-capd
  artifacts:
    expose_as: kubeadm-capd

deploy-kubeadm-capd-oci:
  extends: deploy-kubeadm-capd
  variables:
    ENV_NAME: kubeadm-capd
    OCI_TAG: $OCI_TAG_FORMAT
  artifacts:
    expose_as: kubeadm-capd-oci

.deploy-capo:
  extends: .deploy-base
  script:
    - !reference [.common-deployment, script_common]
    - !reference [.common-deployment, os_cloud]
    - !reference [.common-deployment, script_capo]
    - ./bootstrap.sh environment-values/${ENV_NAME}
    - !reference [.common-deployment, post_deployment_common]
  rules:
    - if: $CI_MERGE_REQUEST_LABELS =~ /renovate/
      allow_failure: true
      when: manual
    - !reference [.common-deployment, rules]
  tags:
    - $CAPO_PLATFORM_TAG

deploy-rke2-capo:
  timeout: 90min
  extends: .deploy-capo
  variables:
    ENV_NAME: "rke2-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}"
  artifacts:
    expose_as: rke2-capo

deploy-rke2-capo-oci:
  extends: deploy-rke2-capo
  variables:
    ENV_NAME: "rke2-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}-OCI"
    OCI_TAG: $OCI_TAG_FORMAT
  artifacts:
    expose_as: rke2-capo-oci

deploy-kubeadm-capo:
  timeout: 75min
  extends: .deploy-capo
  variables:
    ENV_NAME: "kubeadm-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}"
  artifacts:
    expose_as: kubeadm-capo

deploy-kubeadm-capo-oci:
  extends: deploy-kubeadm-capo
  variables:
    ENV_NAME: "kubeadm-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}-OCI"
    OCI_TAG: $OCI_TAG_FORMAT
  artifacts:
    expose_as: kubeadm-capo-oci

.cleanup-capo:
  stage: delete
  image:
    name: registry.gitlab.com/sylva-projects/sylva-elements/container-images/openstack-client:v0.0.3
  script:
    - !reference [.common-deployment, os_cloud]
    - ./tools/openstack-cleanup.sh capo_cloud "${CAPO_TAG}"
  rules:
    - if: '$CI_COMMIT_TAG && $OCI_TAG'
    - if: $CI_PIPELINE_SOURCE == 'web'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_DEPLOYMENTS'
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  allow_failure: true
  tags:
    - $CAPO_PLATFORM_TAG

cleanup-rke2-capo:
  extends: .cleanup-capo
  variables:
    ENV_NAME: "rke2-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}"
  needs:
    - deploy-rke2-capo

cleanup-kubeadm-capo:
  extends: .cleanup-capo
  variables:
    ENV_NAME: "kubeadm-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}"
  needs:
    - deploy-kubeadm-capo

cleanup-rke2-capo-oci:
  extends: .cleanup-capo
  variables:
    ENV_NAME: "rke2-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}-OCI"
    OCI_TAG: $OCI_TAG_FORMAT # required to match the same rules as OCI based deployments
  needs:
    - deploy-rke2-capo-oci

cleanup-kubeadm-capo-oci:
  extends: .cleanup-capo
  variables:
    ENV_NAME: "kubeadm-capo"
    CAPO_TAG: "${CI_PIPELINE_ID}-${ENV_NAME}-OCI"
    OCI_TAG: $OCI_TAG_FORMAT # required to match the same rules as OCI based deployments
  needs:
    - deploy-kubeadm-capo-oci

scheduled-cleanup-capo:
  stage: delete
  image:
    name: registry.gitlab.com/sylva-projects/sylva-elements/container-images/openstack-client:v0.0.3
  script:
    - !reference [.common-deployment, os_cloud]
    - touch tags.txt exclude-tags.txt
    - |
      for STACK in $(openstack stack list -f value -c "Stack Name" | grep -E '^(management|workload|capo)-cluster' ); do
        STACK_TIME=$(date -d "$(openstack stack show ${STACK} -c creation_time -f value | tr T ' ' | tr -d Z )" +%s)
        THIS_TIME=$(date +%s)
        # we'll only consider for cleanup tags applied to resources created more than 2 hours ago
        if [ $((THIS_TIME-STACK_TIME)) -gt 7200 ]; then
            openstack stack show ${STACK} -c tags -f json | jq .tags[] -r >> tags.txt
        else
            openstack stack show ${STACK} -c tags -f json | jq .tags[] -r >> exclude-tags.txt
        fi
      done
    - |
      # skip the cleanup for resources with tags present in exclude-tags.txt
      comm -3 -2 <(sort tags.txt) <(sort exclude-tags.txt) | sort -u | while read -r CAPO_TAG; do
        echo -e "\e[1m\e[0Ksection_start:`date +%s`:scheduled_cleanup[collapsed=true]\r\e[0K\U0001F5D1  Cleaning tag: ${CAPO_TAG} \e[0m"
        ./tools/openstack-cleanup.sh capo_cloud ${CAPO_TAG}
        echo -e "\e[0Ksection_end:`date +%s`:scheduled_cleanup\r\e[0K"
      done
  variables:
    OS_CLOUD: "capo_cloud"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_CAPO_CLEANUP'
      when: always
  tags:
    - $CAPO_PLATFORM_TAG

.build-base:
  stage: build
  variables:
    OCI_REGISTRY: oci://${CI_REGISTRY_IMAGE}
    OCI_TAG: $OCI_TAG_FORMAT
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        OCI_TAG: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == 'web'
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_DEPLOYMENTS'
    - if: $CI_MERGE_REQUEST_LABELS =~ /run-e2e-tests/
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

publish-kustomize-units-artifact:
  extends: .build-base
  script:
    - tools/oci/build-kustomize-units-artifact.sh ${OCI_TAG}

push-helm-artifacts:
  extends: .build-base
  script:
    - tools/oci/push-helm-charts-artifacts.sh "oci://${CI_REGISTRY_IMAGE}"

publish-sylva-units-artifact:
  extends: .build-base
  script:
    - tools/oci/build-sylva-units-artifact.sh ${OCI_TAG}
